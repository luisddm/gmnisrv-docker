FROM alpine

RUN apk update && \
    apk add --no-cache --virtual .build-deps git gcc make build-base && \
    apk add openssl-dev scdoc mailcap && \
    git clone https://git.sr.ht/~sircmpwn/gmnisrv /app/src && \
    mkdir /app/build && \
    cd /app/build && \
    ../src/configure --prefix=/usr && \
    make && \
    make install && \
    mkdir /srv/gemini && echo "Hello from dockerized gmnisrv" > /srv/gemini/index.gmi && \
    apk del .build-deps && \
    rm -rf /app/src

EXPOSE 1965

WORKDIR /app/build

CMD [ "/app/build/gmnisrv" ]
