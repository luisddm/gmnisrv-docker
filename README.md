# gmnisrv-docker

Dockerización del sevidor de Gemini [gmnisrv](https://sr.ht/~sircmpwn/gmnisrv/). Es necesario tener instalado Docker y docker-compose previamente en el sistema.

## Configuración

- Clonamos este repositorio en nuestro servidor.

- Usamos el fichero [`.env`](.env) para configurar las variables de entorno.
  - `GMNISRV_PORT`: el puerto donde se ejecutará el servidor.
  - `GMNISRV_CONFIG_FILE`: el archivo .ini de configuración.
  - `GMNISRV_FILES_DIR`: el directorio que contiene los archivos que se servirán. Pueden ser tanto archivos .gmi como scripts (estos últimos deberán colocarse en el subdirectorio `cgi-bin`, o en el que configuremos en el .ini).
  - `GMNISRV_CERTS_DIR`: el directorio que contiene los certificados (autogenerados).

- En el fichero de configuración (por defecto [`config/gmnisrv.ini`](config/gmnisrv.ini)) cambiamos `gemini.domain.org` por el dominio de nuestro servidor.

- Ejecutamos:

  ```bash
  docker-compose up -d
  ```

  Esto creará el contenedor y lo arrancará. Clonará el repo de gmnisrv dentro del contenedor, instalará todas las dependencias necesarias, compilará el servidor de Gemini, hará una limpieza y lo arrancará.

- Ya podemos acceder desde cualquier navegador Gemini a nuestro servidor mediante la url que hayamos configurado previamente (siguiendo con el ejemplo, sería `gemini://gemini.domain.org`).

Se puede consultar un listado de clientes, servidores, proxies y mucho más [aquí](https://github.com/kr1sp1n/awesome-gemini).

## Referencias

- Como montar un servidor Gemini (gemini://michan.noho.st/tutoriales/howto.gmi).

- Repositorio del proyecto gmnisrv (https://sr.ht/~sircmpwn/gmnisrv).

## Más info

- Proxy para acceder a Gemini o Gopher a través de la web (https://proxy.vulpes.one).

- Guía para escribir archivos Gemtext (.gmi) (https://admin.flounder.online/gemini_text_guide.gmi).

- FAQ de Gemini (https://gemini.circumlunar.space/docs/faq.gmi).

- Especificacíón del protocolo Gemini (https://gemini.circumlunar.space/docs/specification.html).
